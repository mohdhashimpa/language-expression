var async = require('async');
var elasticsearch = require('elasticsearch');
var http = require('http');
var fs = require('fs');



giveRequest(function(err,componentNames){
    if(err){
        console.log(err)
    }
    else{
        getFileContents(componentNames)
    }
});

function giveRequest(callback) {

    return http.get({
        host: '96.84.49.11',
        port: '8080',
        path: '/inference/component?type=tag'
    }, function(response) {
        var body = '';
        response.on('data', function(d) {
            body += d;
        });
        response.on('end', function() {
            body = JSON.parse(body)
          var components = body.components;
          var componentNames=[];
          for(index in components){
            componentNames.push(components[index].componentName)
          }
          callback(null,componentNames)
        });
    });
}

function getFileContents(componentNames){
    var contents = 
    "Expression =\r\n\t condition: ConditionParser \r\n     \r\nConditionParser =  CONDITION _ TAG _ TAG_VALUES _ ConditionParser\r\n\t\t\t\t  /CONDITION _ TAG _ TAG_VALUES\r\n\r\nCONDITION = \"IF\"i\r\n\t\t\t/\"AND IF\"i\r\n            /\"OR IF\"i\r\n            /\"EXPECT IF\"i\r\nTAG = \"TAG\"i\t\r\n\r\nWORD = word:[a-zA-Z]+ { return text()}\r\n\r\n_ \"whitespace\"\r\n  = [ \\t\\n\\r]* {return}\r\n\r\n\r\nInteger \"integer_Suggetion\"\r\n  = [0-9]+ { return parseInt(text(), 10); }";
    contents+="\n\nTAG_VALUES = ";
    for(var i in componentNames){
        if(i==0){
            contents+="\""+componentNames[i].split(" ").join("\"i_\"")+"\""+"i"+"\n"
        }
        else{
            contents+="/"+"\""+componentNames[i].split(" ").join("\"i_\"")+"\""+"i"+"\n"

        }
    }
    writeToFile(contents)

}

function writeToFile(contents){
    fs.writeFile("rules.peg", contents , function(err) {
        if(err) {
            return console.log(err);
        }

        console.log("The file was saved!");
    });
}