import React from 'react';
import { hashHistory,Link } from 'react-router'
import 'public/css/bootstrap.css'
import 'public/css/style.css'
import 'public/css/search.css'
import Reactable from "reactable"
import _ from "lodash"
import AutoComplete from 'material-ui/AutoComplete';
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import MenuItem from 'material-ui/MenuItem';
import Warning from 'material-ui/svg-icons/alert/warning';

var parser = require("./parser");

var AutoCompleteComponent = React.createClass({
	getInitialState: function() {
		var autoComplete = [];
		try {
			parser.parse("");
		}
		catch(e){
			autoComplete = _.map(e.expected,function(item){
				return item.text;
			});
		}
    	return {
    		dataSource : autoComplete,
			isInitial: true,
			selectedText : "",
			open : false
    	};
  	},
	getChildContext: function() {
        return {
            muiTheme:  getMuiTheme(baseTheme)
        };
    },
	childContextTypes: {
        muiTheme: React.PropTypes.object
    },
	handleUpdateInput : function(query)  {
		// query = _.trim(query);
		// console.log("QUERY : "+query);
		try{
			var queryResponse = parser.parse(query,{ isLookup : true});
			// var queryResponse = parser.parse(query,{ isLookup : false});
			// console.log(queryResponse);
			// this.refs.autoComplete.focus();
		}
		catch(e){
			// console.log("Exception message : ",e.message);
			// console.log("Exception : ",JSON.stringify(e));
			var autoComplete = [];
			if(e.hasOwnProperty("location")){
                var start = e.location.start.offset;
                if(e.found){
                    start = query.lastIndexOf(e.found)
                }
                var prefix = query.substring(0,start);
                var candidatePrefix = query.substring(start);
            }
			if(e.expected[0].type == "other" ) {
				if(e.expected[0].description.endsWith("_Complete")){
					var key = e.expected[0].description;
					key = key.replace("_Complete","");
					var values = lookup[key];
					autoComplete = _.map(values,function(value){
						return prefix +" "+ value;
					});
					autoComplete = _.compact(autoComplete)
				}
				else if(e.expected[0].description.endsWith("_Suggetion")){
					var key = e.expected[0].description;
					key = key.replace("_Suggetion","");
					autoComplete.push( {
						text: "Please enter the "+key,
						value: (
							<MenuItem style={{color: '#FF6F00'}} primaryText={"Please the "+key} leftIcon={<Info color='#FF6F00'/>} />
						),
					});
				}
			}
			else {
				for(var itr in e.expected){
					var text = e.expected[itr].text;
					if(!text){
						continue;
					}
					if(this.state.isInitial){
						autoComplete.push(text);
					}
					else{
						if(text.toLowerCase().startsWith(candidatePrefix.toLowerCase())){
							if(prefix===""){
								autoComplete.push(text);
							}
							else {
								if(text){
									autoComplete.push(prefix + " " + text);
								}
							}
						}
					}
				}
			}
			autoComplete = _.compact(autoComplete)
			autoComplete = _.uniq(autoComplete);
			if(autoComplete.length===0){
				autoComplete.push( {
					text: "We dont understand you",
					value: (
						<MenuItem style={{color: 'red'}} primaryText={"We dont understand you"} leftIcon={<Warning color='red' />} />
					),
				})
			}
			this.setState({
				dataSource: autoComplete,
				isInitial : false,
				open :true
			});
		}
	},
	onSelect: function(text){
		this.state.isInitial = false;
		this.handleUpdateInput(text);
		this.refs.autoComplete.focus();
	},
	handleBlur : function(){

	},
	render: function(){
		console.log("STATE OF OPEN : "+this.state.open);
			return (
				<AutoComplete
					searchText={this.state.selectedText}
					ref='autoComplete'
					filter  ={AutoComplete.noFilter}
					dataSource={this.state.dataSource}
					onUpdateInput={this.handleUpdateInput}
					floatingLabelText="Smart Search"
					fullWidth={true}
					openOnFocus={true}
					open={this.state.open}
					onNewRequest={this.onSelect}
					onBlur={this.handleBlur}
				/>
          	);
		}
});

module.exports = AutoCompleteComponent;
