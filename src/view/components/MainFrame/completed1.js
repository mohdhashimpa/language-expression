import React from 'react';
import { hashHistory,Link } from 'react-router'
import 'public/css/bootstrap.css'
import 'public/css/style.css'
import 'public/css/search.css'
import Reactable from "reactable"
import _ from "lodash"
import SearchInput from 'react-search-input'


var Table = Reactable.Table;


import SearchAction from 'components/MainFrame/action/onSearch';

import SearchResultStore from 'components/MainFrame/store/SearchResultStore';

var MainFrame = React.createClass({
	getInitialState: function() {
    	return {
    		keyword : null,
    		responses : []
    	};
  	},
  	componentWillMount : function(){
  		SearchResultStore.bind(this.responseProcessor);
  		SearchAction.search(this.state.keyword);
  	},
  	componentWillUnmount : function(){
  		SearchResultStore.unbind(this.responseProcessor);
  	},
	search : function(keyword){
		SearchAction.search(keyword);
	},
	responseProcessor : function(){
		var hits = SearchResultStore.getResponse().hits.hits
		var docs = _.map(hits,function(hit){
			return hit._source
		})
		this.setState({
			responses : docs 
		})
	},
	render: function(){
			return (
      			<div>
        			<SearchInput className="search-input" onChange={this.search} />
					<Table className="table" data={this.state.responses} />
				</div>
          	);
		}
});

module.exports = MainFrame;
