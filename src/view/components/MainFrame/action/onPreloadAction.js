import AppDispatcher from 'dispatcher/dispatcher';
import Constants from 'components/MainFrame/constants/PreloadConstants';
import config from 'utils/config'

var onPreloadAction = function(){

}


onPreloadAction.prototype = {
	 preloadData:function(urlObject){
	 	var urlObject = urlObject?urlObject:{}
		    $.ajax({
					url: config.server + 'preloadFields',
					type: 'POST',
					dataType: 'JSON',
					contentType: "application/json; charset=utf-8",
					data: JSON.stringify(urlObject),
					success: function(resp){
						console.log("onPreloadAction",resp)
							AppDispatcher.dispatch({
					        actionType: Constants.PRELOAD_RESPONSE_RECIEVED,
					        data: resp
					   });
					},
					error: function(err){
						console.log("Search Results: Ajax error ", err);
					}
				});
	 }
}


module.exports = new onPreloadAction();
